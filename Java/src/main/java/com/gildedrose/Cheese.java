package com.gildedrose;

public class Cheese extends Item {

	public Cheese(String name, int sellIn, int quality) {
		super(name, sellIn, quality);
	}

	public void qualityUpdate() {
		if (this.quality < 50) {
        this.quality++;
		}
	}

}
