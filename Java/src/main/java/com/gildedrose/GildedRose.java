package com.gildedrose;

class GildedRose {

	Item[] items;

	public GildedRose(Item[] items) {
		this.items = items;
	}

	public void updateQuality() {
		for (Item item : this.items) {
			if (item.getClass() == CommonItem.class) {
				((CommonItem) item).qualityUpdate();
			} else if (item.getClass() == Cheese.class) {
				((Cheese) item).qualityUpdate();
			} else if (item.getClass() == Pass.class) {
				((Pass) item).qualityUpdate();
			}

			if (item.getClass() != LegendaryItem.class) {
				item.sellIn = item.sellIn - 1;
			}
		}
	}

}

// passé en TDD, tester la couverture, passer par une changement de code en fonction des objects utilisés