package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

	@Test
	void whenCommonItemIsUpdate_thenQualityDecreaseBy1() {
		// Given
		Item[] items = new Item[]{new CommonItem("elixir of life", 10, 10)};

		// When
		GildedRose app = new GildedRose(items);
		app.updateQuality();

		// Then
		CommonItem expectedItem = new CommonItem("elixir of life", 9, 9);
		assertEquals(app.items[0].name, expectedItem.name);
		assertEquals(app.items[0].quality, expectedItem.quality);
		assertEquals(app.items[0].sellIn, expectedItem.sellIn);
	}

	@Test
	void whenCommonItemIsUpdateAndSellInNegative_thenQualityDecreaseTwice() {
		// Given
		Item[] items = new Item[]{new CommonItem("elixir of life", -1, 10)};

		// When
		GildedRose app = new GildedRose(items);
		app.updateQuality();

		// Then
		CommonItem expectedItem = new CommonItem("elixir of life", -2, 8);
		assertEquals(app.items[0].name, expectedItem.name);
		assertEquals(app.items[0].quality, expectedItem.quality);
		assertEquals(app.items[0].sellIn, expectedItem.sellIn);
	}

	@Test
	void whenCheeseIsUpdate_thenQualityIsUpgrade() {
		// Given
		Item[] items = new Item[]{new Cheese("Aged Brie", 10, 10)};
		// When
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		// Then
		Cheese expectedItem = new Cheese("Aged Brie", 9, 11);
		assertEquals(app.items[0].name, expectedItem.name);
		assertEquals(app.items[0].quality, expectedItem.quality);
		assertEquals(app.items[0].sellIn, expectedItem.sellIn);
	}

	@Test
	void whenPassIsUpdate_thenQualityIsUpgrade() {
		// Given
		Item[] items = new Item[]{new Pass("Backstage passes to a TAFKAL80ETC concert", 11, 10)};
		// When
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		// Then
		Pass expectedItem = new Pass("Backstage passes to a TAFKAL80ETC concert", 10, 11);
		assertEquals(app.items[0].name, expectedItem.name);
		assertEquals(app.items[0].quality, expectedItem.quality);
		assertEquals(app.items[0].sellIn, expectedItem.sellIn);
	}

	@Test
	void whenPassIsUpdateAndSellInEqual10_thenQualityIsUpgradeTwice() {
		// Given
		Item[] items = new Item[]{new Pass("Backstage passes to a TAFKAL80ETC concert", 10, 10)};
		// When
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		// Then
		Pass expectedItem = new Pass("Backstage passes to a TAFKAL80ETC concert", 9, 12);
		assertEquals(app.items[0].name, expectedItem.name);
		assertEquals(app.items[0].quality, expectedItem.quality);
		assertEquals(app.items[0].sellIn, expectedItem.sellIn);
	}

	@Test
	void whenPassIsUpdateAndSellInEqual5_thenQualityIsUpgradeThreeTime() {
		// Given
		Item[] items = new Item[]{new Pass("Backstage passes to a TAFKAL80ETC concert", 5, 10)};
		// When
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		// Then
		Pass expectedItem = new Pass("Backstage passes to a TAFKAL80ETC concert", 4, 13);
		assertEquals(app.items[0].name, expectedItem.name);
		assertEquals(app.items[0].quality, expectedItem.quality);
		assertEquals(app.items[0].sellIn, expectedItem.sellIn);
	}

	@Test
	void whenPassIsUpdateAndSellInIsNegative_thenQualityIsEqual0() {
		// Given
		Item[] items = new Item[]{new Pass("Backstage passes to a TAFKAL80ETC concert", 0, 10)};
		// When
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		// Then
		Pass expectedItem = new Pass("Backstage passes to a TAFKAL80ETC concert", -1, 0);
		assertEquals(app.items[0].name, expectedItem.name);
		assertEquals(app.items[0].quality, expectedItem.quality);
		assertEquals(app.items[0].sellIn, expectedItem.sellIn);
	}

	@Test
	void whenCheeseIsUpdateAndQualityIs50_thenQualityIsNotUpgrade() {
		// Given
		Item[] items = new Item[]{new Cheese("Aged Brie", 10, 50)};
		// When
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		// Then
		Cheese expectedItem = new Cheese("Aged Brie", 9, 50);
		assertEquals(app.items[0].name, expectedItem.name);
		assertEquals(app.items[0].quality, expectedItem.quality);
		assertEquals(app.items[0].sellIn, expectedItem.sellIn);
	}

}
